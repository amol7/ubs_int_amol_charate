package com.ubs.opsit.interviews;

import java.util.Arrays;
import java.util.Collections;

/**
 * Berlin Clock 
 */
public class BerlinClock {

    private String formattedTime;    
    private static final String NO_TIME_ERROR = "No time entered";
    private static final String INVALID_TIME_ERROR = "Invalid time entered.";
    private static final String NUMERIC_TIME_ERROR = "Time values must be numeric.";

    /**
     * @param time - The 24 hour time in the format of HH:MM:SS
     */
    public BerlinClock(String time) {

        if(time == null) throw new IllegalArgumentException(NO_TIME_ERROR);

        String[] times = time.split(":", 3);

        if(times.length != 3) throw new IllegalArgumentException(INVALID_TIME_ERROR);

        int hours, minutes, seconds = 0;
        
        try {
            hours = Integer.parseInt(times[0]);
            minutes = Integer.parseInt(times[1]);
            seconds = Integer.parseInt(times[2]);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(NUMERIC_TIME_ERROR);
        }

        if (hours < 0 || hours > 23) throw new IllegalArgumentException("Please enter valid hrs");
        if (minutes < 0 || minutes > 59) throw new IllegalArgumentException("Please enter valid mins");
        if (seconds < 0 || seconds > 59) throw new IllegalArgumentException("Please enter valid secs");

        TimeConverter timeConverter =new TimeConverterImpl();
        this.formattedTime = timeConverter.processTime(hours, minutes, seconds);
    }

   /**
     * Print a representation of the berlin time.
     */
    public void printClock() {
        System.out.println(this);
    }

    @Override
    public String toString() {
        return this.formattedTime;
    }


}
